package
{

	import com.adobe.serialization.json.JSONEncoder;
	
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.ColorTransform;
	import flash.geom.Matrix;
	import flash.net.FileFilter;
	import flash.utils.getQualifiedClassName;
	
	public class LevelGenerator extends Sprite
	{
		private var mc:MovieClip;
		private var c:MovieClip;
		private var ct:ColorTransform;

		private var levels:Array;

		private var _btnSourcePath:UiButtonSWC;
		private var _browseForFile:File;
		
		public function LevelGenerator()
		{
			this.stage.align = StageAlign.TOP_LEFT;
			this.stage.scaleMode = StageScaleMode.NO_SCALE;
			
			var obj:Object;

			levels = [	
						//Level Select Kingdom 1
						{id:"1_0_0", mc:new l_1_0_0()},
				
						//Levels
						{id:"1_1_1", mc:new l_1_1_1()},
						{id:"1_1_2", mc:new l_1_1_2()},
						{id:"1_1_3", mc:new l_1_1_3()},
						{id:"1_1_4", mc:new l_1_1_4()},
						{id:"1_1_5", mc:new l_1_1_5()}, 
						
						{id:"1_2_6", mc:new l_1_2_6()},
						{id:"1_2_7", mc:new l_1_2_7()},
						{id:"1_2_8", mc:new l_1_2_8()},
						{id:"1_2_9", mc:new l_1_2_9()},
						{id:"1_2_10", mc:new l_1_2_10()},
						
						{id:"1_3_11", mc:new l_1_3_11()},
						{id:"1_3_12", mc:new l_1_3_12()},
						{id:"1_3_13", mc:new l_1_3_13()},
						{id:"1_3_14", mc:new l_1_3_14()},
						{id:"1_3_15", mc:new l_1_3_15()},
						
						{id:"1_4_16", mc:new l_1_4_16()},
						{id:"1_4_17", mc:new l_1_4_17()},
						{id:"1_4_18", mc:new l_1_4_18()},
						{id:"1_4_19", mc:new l_1_4_19()},
						{id:"1_4_20", mc:new l_1_4_20()},
						
						{id:"1_5_21", mc:new l_1_5_21()},
						{id:"1_5_22", mc:new l_1_5_22()},
						{id:"1_5_23", mc:new l_1_5_23()},
						{id:"1_5_24", mc:new l_1_5_24()},
						{id:"1_5_25", mc:new l_1_5_25()},
						
						// UI Levels
						{id:"achievementsLevel", mc:new AchievementsScreenSWC()},
						{id:"optionsLevel", mc:new ContactUsScreenSWC()}, //overlay
						{id:"kingdomSelectLevel", mc:new KingdomSelectScreenSWC()},
						{id:"shopLevel", mc:new AchievementsScreenSWC}, //overlay
						{id:"menuLevel", mc:new MenuScreenSWC},
						{id:"treasureLevel", mc:new LostTreasuresScreenSWC},
						{id:"welcomeLevel", mc:new LandingScreenSWC},
						{id:"introLevel", mc:new LandingScreenSWC()}
			];
			
			_btnSourcePath = new UiButtonSWC();
			_btnSourcePath.tfLabel.text = "Set Path";
			_btnSourcePath.buttonMode = true;
			_btnSourcePath.mouseChildren = false;
			this.addChild(_btnSourcePath);
		
			this.addEventListener(MouseEvent.CLICK, onButton);
		}
		
		protected function onButton(event:MouseEvent):void
		{
			switch(event.target)
			{
				case _btnSourcePath:
					_browseForFile = File.applicationStorageDirectory;
					_browseForFile.addEventListener(Event.SELECT, selectHandler);
					_browseForFile.browseForDirectory("Select Output Directory");
					break;
			}
			
		}
		
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		
		private function getTypes():Array {
			var allTypes:Array = new Array(getImageTypeFilter(), getTextTypeFilter());
			return allTypes;
		}
		
		private function getImageTypeFilter():FileFilter {
			return new FileFilter("Images (*.jpg, *.jpeg, *.gif, *.png)", "*.jpg;*.jpeg;*.gif;*.png");
		}
		
		private function getTextTypeFilter():FileFilter {
			return new FileFilter("Text Files (*.txt, *.rtf)", "*.txt;*.rtf");
		}
		private function selectHandler(event:Event):void {
			
			if(event.target is File)
			{
				_browseForFile = event.target as File;
				start();
			}
		}
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		
		private function start():void
		{
			this.addEventListener(Event.ENTER_FRAME, loop);
		}
		
		private function loop(e:Event=null):void
		{
			this.removeEventListener(Event.ENTER_FRAME, loop);
			
			for(var l:int=0;l<levels.length;l++)
			{
				generateJSon(levels[l]);
			}
		}
		
		private function generateJSon(obj:Object):void
		{
			trace("level "+mc+"------------------------------------------------------------------------------------");
			/*mc = e.target as MovieClip;
			mc.removeEventListener(Event.ENTER_FRAME, generateJSon);*/
			mc = obj.mc;
			obj = {main:[],physics:[],character:"0",foreground:[], mo:[], pa:[],level:obj.id, checkpoints:[], areaCenters:[], slimiesCount:0};
			
			var l:Number = mc.numChildren;
			var physics:Array = [];
			
			var state:String = "main";
			var params:Object = {};
			var slimies:Number = 0;
			
			for(var i:int=0;i<l;i++)
			{
				c = mc.getChildAt(i) as MovieClip;
				params = {};
				if (c.params) 
				{
					params = c.params;
				}
				
				for (var metatags:String in c)
				{
					if (metatags != "componentInspectorSetting" && metatags != "className") {
						params[metatags] = c[metatags];
					}
				}
				
				var t:Matrix = c.transform.matrix;
				if(c is MovieClip)
				{
					if(c.name == "divider_paralax")
					{
						state = "paralax";
						continue;
					} else if(c.name == "divider_physics")
					{
						state = "physics";
						continue;
					} else if(c.name == "divider_foreground")
					{
						state = "foreground";
						continue;
					} else if(c.name == "divider_character")
					{
						state = "character";
						continue;
					} else if(c.name == "divider_movingObjectsFront")
					{
						state = "movingObjectsFront";
						continue;
					} else if(c.name == "divider_main")
					{
						state = "main";
						continue;
					} else if(c.name == "divider_movingObjectsBack")
					{
						state = "movingObjectsBack";
						continue;
					}
					
					var rott:Number = -Math.atan(t.c/t.d);
					var ssx:Number = trimNumber(Math.sqrt((t.a*t.a)+(t.b*t.b)) * ((t.a >= 0) ? 1 : -1));
					var ssy:Number = trimNumber(Math.sqrt((t.c*t.c)+(t.d*t.d)) * ((t.d >= 0) ? 1 : -1));
					
					switch(state)
					{
						case "main":
							
							obj.main.push({i:""+getQualifiedClassName(c).split("::").join("."), 
								sx:ssx, 
								sy:ssy, 
								r:trimNumber(rott), 
								x:t.tx, 
								y:t.ty,
								params:params});
							break;
						case "physics":
							var rot:Number = c.rotation;
							c.rotation = 0;
							obj.physics.push({name:c.name,i:""+getQualifiedClassName(c).split("::").join("."), x:c.x, y:c.y, w:c.width, h:c.height,r:rot, c:c["className"],params:params}); 
							c.rotation = rot;
							break;
						case "foreground":
							//obj.foreground.push({id:""+getQualifiedClassName(c), m:[t.a,t.b,t.c,t.d,t.tx,t.ty]}); 
							break;
						case "movingObjectsFront":
							var oldR:Number = c.rotation;
							c.rotation = 0;
							params.x = c.x;
							params.y = c.y;
							params.height = c.height;
							params.width = c.width;
							params.rotation =oldR;
							
							// Count Slimies
							if(c["className"] == "SlimeCollectible" ||c["className"] == "SuperSlimeCollectible")
							{
								slimies++;
							}
							
							if(c is AreaCenter)
							{
								obj.areaCenters.push({params:params});
							} else
							{
								obj.mo.push({name:c.name, c:c["className"], params:params}); 
							}
							break;
						case "paralax":
							obj.pa.push({i:""+getQualifiedClassName(c).split("::").join("."), 
								sx:ssx, 
								sy:ssy, 
								r:trimNumber(rott), 
								x:t.tx, 
								y:t.ty,
								params:params});
							break;
						case "character":
							obj.character = i;
							break;
						case "movingObjectsBack":
							obj.mo.push({name:c.name,x:c.x,y:c.y,r:c.rotation, c:c["className"]}); 
							break;
					}
				}
			}
			trace("slimies: "+slimies)
			obj.slimiesCount = slimies;
			
			var je:JSONEncoder = new JSONEncoder(obj);
			var str:String = je.getString();
			
			outFile(obj.level+".json",str);
			
			//tfLevelData.text = "";
//			tfLevelData.appendText(str);
//			tfLevelData.appendText("---------------------------------------------------------------------------------------------------");
			trace("---------------------------------------------------------------------------------------------------");
		}
		
		private function trimNumber(n:Number):Number
		{
			return Math.floor(n*100000)/100000;
		}
		
		private function outFile(fileName:String, data:String):File
		{
			var outFile:File = _browseForFile; // dest folder is desktop
			outFile = outFile.resolvePath(fileName);  // name of file to write
			var outStream:FileStream = new FileStream();
			// open output file stream in WRITE mode
			outStream.open(outFile, FileMode.WRITE);
			// write out the file
//			outStream.writeBytes(data, 0, data.length);
			outStream.writeUTFBytes(data);
			// close it
			outStream.close();
			return outFile;
		}
	}
}