package
{
	import com.adobe.serialization.json.JSONEncoder;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.net.FileFilter;
	
	public class ScreensGenerator extends Sprite
	{
		private var screens:Array;
		private var _btnSourcePath:UiButtonSWC;
		private var _browseForFile:File;
		
		private var sw:Number = 480;
		private var sh:Number = 320;
		
		private var mc:MovieClip;
		private var c:MovieClip;
		private var ct:ColorTransform;
		private var jsonObj:Object;
		private var params:Object;
		
		public function ScreensGenerator()
		{
			this.stage.align = StageAlign.TOP_LEFT;
			this.stage.scaleMode = StageScaleMode.NO_SCALE;
			
			screens = [
				{id:"LevelResult", mc:new LevelResultScreenSWC()},
				{id:"Hud", mc:new HudScreenSWC()},
				{id:"GaugeUpgrade", mc:new GaugeUpgradeScreenSWC()},
				{id:"ConfirmScreen", mc:new ConfirmScreenSWC()},
				{id:"InventoryScreen", mc:new InventoryScreenSWC()},
				{id:"KingdomSelectScreen", mc:new KingdomSelectionScreenSWC()},
				{id:"OptionsScreen", mc:new SystemOptionsScreenSWC()},
				{id:"MenuScreen", mc:new MenuScreenSWC()},
				{id:"ShopScreen", mc:new ShopScreenSWC()},
				{id:"TreasureScreen", mc:new TreasureScreenSWC()},
				{id:"AchievementsScreen", mc:new AchievementsScreenSWC()},
				{id:"LevelSelectScreen", mc:new LevelSelectionScreenSWC()},
				{id:"WelcomeScreen", mc:new WelcomeScreenSWC()},
				{id:"MessageScreen", mc:new MessageScreenSWC()},
				{id:"InfoScreen", mc:new InfoScreenSWC()},
				{id:"StoryScreenMain", mc:new StoryScreenMainSWC()}
			];
			jsonObj = {screens:[]};
			
			_btnSourcePath = new UiButtonSWC();
			_btnSourcePath.tfLabel.text = "Set Screens";
			_btnSourcePath.buttonMode = true;
			_btnSourcePath.mouseChildren = false;
			this.addChild(_btnSourcePath);
			
			this.addEventListener(MouseEvent.CLICK, onButton);
		}
		
		protected function onButton(event:MouseEvent):void
		{
			switch(event.target)
			{
				case _btnSourcePath:
					_browseForFile = File.applicationStorageDirectory;
					_browseForFile.addEventListener(Event.SELECT, selectHandler);
					_browseForFile.browseForDirectory("Select Output Directory");
					break;
			}
		}
		
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		
		private function getTypes():Array {
			var allTypes:Array = new Array(getImageTypeFilter(), getTextTypeFilter());
			return allTypes;
		}
		
		private function getImageTypeFilter():FileFilter {
			return new FileFilter("Images (*.jpg, *.jpeg, *.gif, *.png)", "*.jpg;*.jpeg;*.gif;*.png");
		}
		
		private function getTextTypeFilter():FileFilter {
			return new FileFilter("Text Files (*.txt, *.rtf)", "*.txt;*.rtf");
		}
		private function selectHandler(event:Event):void {
			
			if(event.target is File)
			{
				_browseForFile = event.target as File;
				start();
			}
		}
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		
		private function start():void
		{
			this.addEventListener(Event.ENTER_FRAME, loop);
		}
		
		private function loop(e:Event=null):void
		{
			this.removeEventListener(Event.ENTER_FRAME, loop);
			
			for(var l:int=0;l<screens.length;l++)
			{
				generateScreenJSon(screens[l]);
			}
			
			var je:JSONEncoder = new JSONEncoder(jsonObj);
			var str:String = je.getString();
			
			outFile("screensData.json",str);
			
			trace("COMPLETE!!! ");
		}
		
		private function generateScreenJSon(screen:Object):void
		{
			//trace("Screen "+screen.id+"------------------------------------------------------------------------------------");
			var obj:Object = {id:screen.id, items:[]};
			// Loop through levels
			mc = screen.mc;
			var it:Number = mc.numChildren;
			var p:DisplayObject;
			var xPos:Number;
			var yPos:Number;
			var point:Point;
			var point2:Point;
			var className:String;
			
			
			for(var i:int=0;i<it;i++)
			{
				c = mc.getChildAt(i) as MovieClip;
				params = {};
				if (c.params) 
				{
					params = c.params;
				}
				
				for (var metatags:String in c)
				{
					if (metatags != "componentInspectorSetting") {
						params[metatags] = c[metatags];
					}
				}
				point = new Point(c.x,c.y);
				if(c["parentClip"] && c["parentClip"] != "")
				{
					p = mc.getChildByName(c["parentClip"]);
					point2.x = point.x - p.x;
					point2.y = point.y - p.y;
				} else
				{
					point2 = point;
				}
				if(c is MovieClip)
				{
					obj.items.push({name:c.name, width:c.width, height:c.height, r:c.rotation, xPer:point2.x/sw, yPer:point2.y/sh, x:point2.x, y:point2.y, params:params});
				}
			}
			
			jsonObj.screens.push(obj);
		}
		
		private function trimNumber(n:Number):Number
		{
			return Math.floor(n*100000)/100000;
		}
		
		private function outFile(fileName:String, data:String):File
		{
			var outFile:File = _browseForFile; // dest folder is desktop
			outFile = outFile.resolvePath(fileName);  // name of file to write
			var outStream:FileStream = new FileStream();
			// open output file stream in WRITE mode
			outStream.open(outFile, FileMode.WRITE);
			// write out the file
			//			outStream.writeBytes(data, 0, data.length);
			outStream.writeUTFBytes(data);
			// close it
			outStream.close();
			return outFile;
		}
	}
}
