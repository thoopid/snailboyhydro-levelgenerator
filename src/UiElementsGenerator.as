package
{
	import com.adobe.serialization.json.JSONEncoder;
	
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.geom.ColorTransform;
	import flash.geom.Point;
	import flash.net.FileFilter;
	import flash.utils.getQualifiedClassName;
	
	public class UiElementsGenerator extends Sprite
	{
		private var uiElements:Array;
		private var _btnSourcePath:UiButtonSWC;
		private var _browseForFile:File;
		
		private var sw:Number = 480;
		private var sh:Number = 320;
		
		private var mc:MovieClip;
		private var c:MovieClip;
		private var ct:ColorTransform;
		private var jsonObj:Object;
		private var params:Object;
		
		public function UiElementsGenerator()
		{
			this.stage.align = StageAlign.TOP_LEFT;
			this.stage.scaleMode = StageScaleMode.NO_SCALE;
			
			uiElements = [
				{id:"AreaElement_1_1", mc:new AreaElement_shardsSWC},
				{id:"AreaElement_1_2", mc:new AreaElement_pipesSWC},
				{id:"AreaElement_1_3", mc:new AreaElement_chestSWC},
				{id:"AreaElement_1_4", mc:new AreaElement_tombSWC},
				{id:"AreaElement_1_5", mc:new AreaElement_scrollSWC},
				{id:"KingdomElement_1", mc:new KingdomElement_1SWC},
				{id:"GaugeView", mc:new GaugeViewSWC},
				{id:"KingdomSelectItem", mc:new KingdomSelectItemSWC},
				{id:"CaveSelectItem", mc:new CavelelectItemSWC},
				{id:"ShopItemBase", mc:new ShopItemBaseHolderSWC},
				
				// Tutorial Assets
				{id:"tut_01", mc:new PullTutorialComponentSWC},
				{id:"tut_02", mc:new SuperJumpTutorialComponentSWC},
				{id:"tut_03", mc:new GaugeSlotsTutorialComponentSWC},
				{id:"tut_04", mc:new StickTutorialSWC},
				{id:"tut_05", mc:new StoryItemTutorialComponent},
				{id:"tut_06", mc:new SecretAreaTutorialComponentSWC},
				{id:"tut_07", mc:new SwimTutorialComponentSWC},
				{id:"tut_09", mc:new ExitTutorialComponentSWC},
				{id:"tut_10", mc:new InventoryOpenTutorialComponentSWC},
				{id:"tut_11", mc:new DragTutorialComponentSWC},
				{id:"tut_12", mc:new InventoryBuyTutComponentSWC},
				{id:"tut_12_2", mc:new InventoryCloseTutorialComponentSWC},
				{id:"tut_13", mc:new SelectLevelTutorialComponentSWC}
			];
			jsonObj = {ui:[]};
			
			_btnSourcePath = new UiButtonSWC();
			_btnSourcePath.tfLabel.text = "GenerateUi";
			_btnSourcePath.buttonMode = true;
			_btnSourcePath.mouseChildren = false;
			this.addChild(_btnSourcePath);
			
			this.addEventListener(MouseEvent.CLICK, onButton);
		}
		
		protected function onButton(event:MouseEvent):void
		{
			switch(event.target)
			{
				case _btnSourcePath:
					_browseForFile = File.applicationStorageDirectory;
					_browseForFile.addEventListener(Event.SELECT, selectHandler);
					_browseForFile.browseForDirectory("Select Output Directory");
					break;
			}
		}
		
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		
		private function getTypes():Array {
			var allTypes:Array = new Array(getImageTypeFilter(), getTextTypeFilter());
			return allTypes;
		}
		
		private function getImageTypeFilter():FileFilter {
			return new FileFilter("Images (*.jpg, *.jpeg, *.gif, *.png)", "*.jpg;*.jpeg;*.gif;*.png");
		}
		
		private function getTextTypeFilter():FileFilter {
			return new FileFilter("Text Files (*.txt, *.rtf)", "*.txt;*.rtf");
		}
		private function selectHandler(event:Event):void {
			
			if(event.target is File)
			{
				_browseForFile = event.target as File;
				start();
			}
		}
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		// 8888888888888888888888888888888888888888888888888888888888888888888888888
		
		private function start():void
		{
			this.addEventListener(Event.ENTER_FRAME, loop);
		}
		
		private function loop(e:Event=null):void
		{
			this.removeEventListener(Event.ENTER_FRAME, loop);
			
			for(var l:int=0;l<uiElements.length;l++)
			{
				generateScreenJSon(uiElements[l]);
			}
			
			var je:JSONEncoder = new JSONEncoder(jsonObj);
			var str:String = je.getString();
			
			outFile("uiElementsData.json",str);
		}
		
		private function generateScreenJSon(uiEl:Object):void
		{
			//trace("Screen "+screen.id+"------------------------------------------------------------------------------------");
			var obj:Object = {id:uiEl.id, items:[]};
			// Loop through levels
			mc = uiEl.mc;
			var it:Number = mc.numChildren;
			var p:DisplayObject;
			var xPos:Number;
			var yPos:Number;
			var point:Point;
			var point2:Point;
//			var className:String;
			
			
			for(var i:int=0;i<it;i++)
			{
				c = mc.getChildAt(i) as MovieClip;
				
				params = {};
				if (mc.params) 
				{
					params = c.params;
				}
				
				for (var metatags:String in c)
				{
					if (metatags != "componentInspectorSetting") {
						params[metatags] = c[metatags];
					}
				}
				
				point = new Point(c.x,c.y);
				point2 = point;
//				className = "";
				if(c is MovieClip)
				{
//					if(params.className)
//					{
//						className = params.className;
//					}
					params.width = c.width;
					params.height = c.height;
//					params.className = className;
					params.name = (c.name.indexOf("instance") != -1) ? getQualifiedClassName(c).split("::").join(".") : c.name;
					params.i = getQualifiedClassName(c).split("::").join(".");
					params.rotation = c.rotation;
					params.x = point2.x;
					params.y = point2.y;
					params.xPer = point2.x / sw;
					params.yPer = point2.y / sh;
					params.scaleX = c.scaleX;
					params.scaleY = c.scaleY;
					params.fixed = c.fixed;
					obj.items.push(params);
				}
			}
			
			jsonObj.ui.push(obj);
		}
		
		private function trimNumber(n:Number):Number
		{
			return Math.floor(n*100000)/100000;
		}
		
		private function outFile(fileName:String, data:String):File
		{
			var outFile:File = _browseForFile; // dest folder is desktop
			outFile = outFile.resolvePath(fileName);  // name of file to write
			var outStream:FileStream = new FileStream();
			// open output file stream in WRITE mode
			outStream.open(outFile, FileMode.WRITE);
			// write out the file
			//			outStream.writeBytes(data, 0, data.length);
			outStream.writeUTFBytes(data);
			// close it
			outStream.close();
			return outFile;
		}
	}
}
